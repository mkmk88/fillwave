/*
 * Profiler.h
 *
 *  Created on: May 5, 2015
 *      Author: Filip Wasil
 */

#ifndef SRC_PROFILER_H_
#define SRC_PROFILER_H_

/*************************************************************************
 *
 * Copyright (C) 2015 Filip Wasil
 *
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and may be covered by Polish and foreign patents, patents
 * in process, and are protected by trade secret or copyright
 * law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * fillwave@gmail.com
 *
 */

#include <omp.h>
#include <chrono>
#include <thread>

//#define likely(x) __builtin_expect((x),1)
//#define unlikely(x) __builtin_expect((x),0)

//#define FILLWAVE_OPENMP_COPY_VERTEX_BUFFER_LOOP omp parallel for schedule(guided) num_threads(2) if (mTotalElements > 1000) // + omp_get_num_procs()*mTotalElements/100000
//#define FILLWAVE_OPENMP_FLIP_TEXTURE_LOOP omp parallel for schedule(guided) num_threads(4) // + omp_get_num_procs()*mTotalElements/100000
//#define FILLWAVE_OPENMP_COPY_VERTEX_BUFFER_LOOP_ORDERED omp parallel for schedule(guided) ordered num_threads(2) if (mTotalElements > 1000) // + omp_get_num_procs()*mTotalElements/100000

//#define FILLWAVE_START_TIME() std::chrono::high_resolution_clock::now()
//#define FILLWAVE_DELTA_TIME(startTime) std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - startTime).count()

#define FILLWAVE_AO_SAMPLE_RADIUS 1.6f
#define FILLWAVE_OQ_VERTICES 36

#endif /* SRC_PROFILER_H_ */
