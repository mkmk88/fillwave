/*
 * ResourceManager.h
 *
 *  Created on: 15 Apr 2014
 *      Author: wasilfil
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

/*************************************************************************
 *
 * Copyright (C) 2015 Filip Wasil
 *
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and may be covered by Polish and foreign patents, patents
 * in process, and are protected by trade secret or copyright
 * law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * fillwave@gmail.com
 *
 */

#include <fillwave/management/BufferManager.h>
#include <fillwave/management/ProgramManager.h>
#include <fillwave/management/TextureManager.h>
#include <fillwave/management/ShaderManager.h>
#include <fillwave/management/SamplerManager.h>
#include <fillwave/management/LightManager.h>

namespace fillwave {
namespace manager {

/*! \class ResourceManager
 * \brief Not used.
 */

class ResourceManager {
public:
    ResourceManager(std::string& rootPath);
    virtual ~ResourceManager();

    void initBufferManager();
    void initProgramManager();
    void initShapeManager();
    void initTextureManager();
    void initShaderManager();
    void initSamplerManager();

    pProgram getProgram();
    pTexture2D getTexture2D();
    pShader getShader();
    pSampler getSampler();
    pBuffer getBuffer();

private:
   puBufferManager   mBufferManager;
   puProgramManager  mProgramManager;
   puTextureManager  mTextureManager;
   puShaderManager  mShaderManager;
   puSamplerManager  mSamplerManager;
};

} /* manager */
} /* fillwave */

#endif /* RESOURCEMANAGER_H_ */
