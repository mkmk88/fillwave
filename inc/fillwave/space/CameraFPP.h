/*
 * CameraFPP.h
 *
 *  Created on: Oct 9, 2014
 *      Author: Filip Wasil
 */

#ifndef CAMERAFPP_H_
#define CAMERAFPP_H_

/*************************************************************************
 *
 * Copyright (C) 2015 Filip Wasil
 *
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and may be covered by Polish and foreign patents, patents
 * in process, and are protected by trade secret or copyright
 * law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * fillwave@gmail.com
 *
 */

#include <fillwave/space/Camera.h>

namespace fillwave {
namespace space {

/*! \class CameraFPP
 * \brief Not used.
 */

class CameraFPP: public Camera {
public:
   CameraFPP();
   virtual ~CameraFPP();
   };

} /* space */
} /* fillwave */

#endif /* CAMERAFPP_H_ */
