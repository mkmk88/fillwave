#version 300 es
precision mediump float;
layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec3 aNormalTangent;
layout(location = 4) in vec2 aTextureCoordinate;
layout(location = 5) in ivec4 aBoneID; /* Will be optimized if not used */
layout(location = 6) in vec4 aWeight;  /* Will be optimized if not used */

out vec4 vColor;
out vec2 vTextureCoordinate;
out vec3 vVertexNormal;
out vec3 vVertexNormalTangent;
out vec4 vVertexWorldSpace;
out vec3 vCameraPosition;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat3 uNormalViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 uColorMatrix;
uniform vec3 uCameraPosition;

uniform float uTime;

#define MAX_BONES 45

//layout(std140) uniform uBonesUBO {
//   mat4 uBones[MAX_BONES];
//};

uniform mat4 uBones[MAX_BONES];

void main() {
   mat4 BoneTransform = uBones[int(aBoneID.x)] * aWeight.x +
                        uBones[int(aBoneID.y)] * aWeight.y +
                        uBones[int(aBoneID.z)] * aWeight.z +
                        uBones[int(aBoneID.w)] * aWeight.w;

   vVertexNormal = (uModelMatrix * BoneTransform * vec4(aNormal, 0.0)).xyz;
   vVertexNormalTangent = (uModelMatrix * BoneTransform * vec4(aNormalTangent, 0.0)).xyz;
   vVertexWorldSpace = uModelMatrix * BoneTransform * aPosition;
   vTextureCoordinate = aTextureCoordinate;
   vCameraPosition = uCameraPosition;
   vColor = aColor;
   gl_Position = uProjectionMatrix * uViewMatrix * vVertexWorldSpace;
}