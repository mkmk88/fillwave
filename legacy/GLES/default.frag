#version 300 es
precision lowp float;
in vec4 vColor;
in vec2 vTextureCoordinate;
in vec3 vVertexNormal;
in vec3 vVertexNormalTangent;
in vec4 vVertexWorldSpace;
in vec3 vCameraPosition;

out vec4 fColor;

/* Lights */
#define MAX_SPOT_LIGHTS 4
#define MAX_POINT_LIGHTS 4
#define MAX_DIRECTIONAL_LIGHTS 4


struct LightPoint {
   vec4 position;
   vec4 intensity;
   mat4 mvp;
};

/* UNIFORMS */

/* Light ambient */
uniform vec4 uLightAmbientIntensity;
uniform vec4 uLightDiffuseIntensity;
uniform vec4 uLightSpecularIntensity;

/* Light point */
//uniform lowp samplerCube uPointShadowMap0;
//uniform lowp samplerCube uPointShadowMap1;
//uniform lowp samplerCube uPointShadowMap2;
//uniform lowp samplerCube uPointShadowMap3;

layout(std140) uniform LightPoint uPointLights[MAX_POINT_LIGHTS];

uniform int uNumberOfPointLights;

/* Light spot */
uniform lowp sampler2DShadow uShadowMap0;
uniform lowp sampler2DShadow uShadowMap1;
uniform lowp sampler2DShadow uShadowMap2;
uniform lowp sampler2DShadow uShadowMap3;
uniform lowp sampler2DShadow uShadowMap4;
uniform lowp sampler2DShadow uShadowMap5;
uniform lowp sampler2DShadow uShadowMap6;
uniform lowp sampler2DShadow uShadowMap7;
layout(std140) uniform uSpotLightsUBO {
   LightPoint uSpotLights[MAX_SPOT_LIGHTS + MAX_DIRECTIONAL_LIGHTS];
};

uniform int  uNumberOfSpotLights;

/* Color picking */
uniform bool uColorPicking;

/* Fog effect */
uniform bool uFogEffect;
uniform vec3 uFogColor;
uniform float uFogNearDistance;
uniform float uFogFarDistance;

/* Painter effect */
uniform bool uPainterEffect;
uniform vec4 uPainterColor;

/* Texture only effect */
uniform bool uTextureOnlyEffect;

/* Boost color effect */
uniform bool uBoostColorEffect;
uniform float uBoostColorFactor;

/* texture maps*/
uniform sampler2D uDiffuseTextureUnit;
uniform sampler2D uNormalTextureUnit;
uniform sampler2D uSpecularTextureUnit;

/*function clamp to border */

float clamp_to_border_factor (vec2 coords, bool clamp_to_border) {
   bvec2 out1 = greaterThan (coords, vec2 (1,1));
   bvec2 out2 = lessThan (coords, vec2 (0,0));
   bool do_clamp = (any (out1) || any (out2)) && clamp_to_border;
   return float (!do_clamp);
}

/* function calculateSpotShadow                 */
/*                                              */
/* Returns the shadow factor                    */
/* Determines if the pixel is in shadow or not  */

float calculateSpotShadow(vec4 spotlightPosition, sampler2DShadow spotShadowMap) {
   return textureProj(spotShadowMap, spotlightPosition) * 
      clamp_to_border_factor(spotlightPosition.xy/spotlightPosition.w, true);
}

float calculateSpotShadowPCF(vec4 spotlightPosition, sampler2DShadow spotShadowMap) {
   float nearestPixel = 0.005;
   float x,y,sum=0.0;
   for (x=-1.0;x<=1.0;x+=1.0) {
      for (y=-1.0;y<=1.0;y+=1.0) {
         sum+=textureProj(spotShadowMap, spotlightPosition + vec4(x*nearestPixel, y*nearestPixel,0.0,0.0));
      }
   }
   return sum*0.11*clamp_to_border_factor(spotlightPosition.xy/spotlightPosition.w, true);
}

/* function calculatePointShadow                */
/*                                              */
/* Returns the shadow factor                    */
/* Determines if the pixel is in shadow or not  */

float calculatePointShadow(vec3 toLightdirection, samplerCube pointShadowMap) {

    float SampledDistance = texture(pointShadowMap, toLightdirection).r;
    float Distance = length(toLightdirection);

    if (Distance <= SampledDistance + 0.001)
       return 1.0;
    else
       return 0.1;
}

/* function calcDiffuse                         */
/*                                              */
/* Returns the cosine of the angle of incidence */
/* between the vertex normal and the light      */

float calcDiffuse(vec3 normal, vec3 direction) {
   return clamp( dot(normal, normalize ( direction) ), 0.0, 1.0);
}

void main() {
   /*shadow*/
   float shadowFactor;
   vec4  lightDepth;
   
   /*common for light models*/
   vec4 intensity;
   float attenuation;
   vec4 color;

   /*ambient*/
   vec3 ambientFactor = vec3(1.0, 1.0, 1.0) - uLightAmbientIntensity.xyz;

   /*diffuse*/
   float diffuse;   
   vec4 diffuseSum;
   vec4 colorDiffuse;
   vec3 toLightDirection;
   vec4 texelDiffuse = uLightDiffuseIntensity * texture(uDiffuseTextureUnit, vTextureCoordinate);
   vec3 texelNormal = texture(uNormalTextureUnit, vTextureCoordinate).xyz;
   vec4 texelSpecular = uLightSpecularIntensity * texture(uSpecularTextureUnit, vTextureCoordinate) * 255.0;

   vec3 vertexNormal;

   /* Create TBN normals */
   vec3 Normal = normalize(vVertexNormal);
   vec3 Tangent = normalize(vVertexNormalTangent);
   
   Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
   vec3 Bitangent = cross(Tangent, Normal);

   /* Check if we use normal matrix or not */
   if (length(texelNormal.xyz) == 0.0) {
      vertexNormal = normalize (Normal);
   } else {
      texelNormal = 2.0 * texelNormal - vec3(1.0, 1.0, 1.0);
      mat3 TBN = ( mat3(Tangent, Bitangent, Normal) );
      vertexNormal = TBN * texelNormal;
      vertexNormal = normalize(vertexNormal);
   }

   /*specular*/
   float specular;
   vec4 specularSum;
   vec4 colorSpecular;
   float toLightDirectionSqr;
   float lightAttenuation = 1.2;
   vec3 viewDirection = normalize(vVertexWorldSpace.xyz - vCameraPosition);
   int realNumberOfSpotLights = MAX_SPOT_LIGHTS + MAX_DIRECTIONAL_LIGHTS;
   int realNumberOfPointLights = MAX_POINT_LIGHTS;

   if ( uPainterEffect || uColorPicking ) {
      color = uPainterColor;
   } else {
      if (uNumberOfSpotLights < MAX_SPOT_LIGHTS + MAX_DIRECTIONAL_LIGHTS) {
         realNumberOfSpotLights = uNumberOfSpotLights;
      }
      if (uNumberOfPointLights < MAX_POINT_LIGHTS) {
         realNumberOfPointLights = uNumberOfPointLights;
      }
      diffuseSum = vec4(0.0);
      specularSum = vec4(0.0);
      shadowFactor = 0.0;
      for (int i = 0; i < realNumberOfSpotLights; i++) { 
         if ( i < realNumberOfSpotLights) {
            if ( i == 0 ) {
               toLightDirection = uSpotLights[0].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[0].intensity * attenuation;
               lightDepth = uSpotLights[0].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap0);
            } else if ( i == 1 ) {
               toLightDirection = uSpotLights[1].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[1].intensity * attenuation;
               lightDepth = uSpotLights[1].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap1);
            } else if ( i == 2 ) {
               toLightDirection = uSpotLights[2].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[2].intensity * attenuation;
               lightDepth = uSpotLights[2].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap2);
            } else if ( i == 3 ) {
               toLightDirection = uSpotLights[3].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[3].intensity * attenuation;
               lightDepth = uSpotLights[3].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap3);
            } else if ( i == 4 ) {
               toLightDirection = uSpotLights[4].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[4].intensity * attenuation;
               lightDepth = uSpotLights[4].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap4);
            } else if ( i == 5 ) {
               toLightDirection = uSpotLights[5].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[5].intensity * attenuation;
               lightDepth = uSpotLights[5].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap5);
            } else if ( i == 6 ) {
               toLightDirection = uSpotLights[6].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[6].intensity * attenuation;
               lightDepth = uSpotLights[6].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap6);
            } else if ( i == 7 ) {
               toLightDirection = uSpotLights[7].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uSpotLights[7].intensity * attenuation;
               lightDepth = uSpotLights[7].mvp * vVertexWorldSpace;
               shadowFactor = calculateSpotShadowPCF(lightDepth, uShadowMap7);
            }

         /* Diffuse */
         diffuse = calcDiffuse(vertexNormal, toLightDirection);

         /* Specular */   
         if (texelSpecular.r == 0.0 || length(texelSpecular.xyz) == 0.0) { //xxx workaround
            texelSpecular.r = 255.0;
         }
         /* Specular Blinn-Phong BRDF calculations */
         /* (Bidirectional Reflectance Distribution Function) */   
         vec3 halfAngle = normalize(normalize(toLightDirection) - normalize(viewDirection));
         specular = dot(vertexNormal, halfAngle);
         specular = clamp(specular, 0.0, 1.0);
         specular = dot(vertexNormal, toLightDirection) != 0.0 ? specular : 0.0;
         specular = pow(specular, texelSpecular.r);
         // shadow map is full of zeros
         // shadowFactor = 1.0;
         diffuseSum += shadowFactor * intensity * diffuse;
         specularSum += shadowFactor * intensity * specular;
         }
      }

      /* POINT LIGHTS - concept
      1) Right vector to read a cubemap is vWorldVertexPosition.xyz - uLightPosition
      2) Shader factory is necessary here.
      */
      float SampledDistance;
      float Distance;
      for (int j = 0; j < realNumberOfPointLights; j++) {
         if ( j < realNumberOfPointLights) {
            if ( j == 0 ) {
               toLightDirection = uPointLights[0].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uPointLights[0].intensity * attenuation;
               SampledDistance = 0.0;//texture(uPointShadowMap0, toLightDirection).r;
               Distance = length(toLightDirection);
               if (Distance <= SampledDistance + 0.001)
                  shadowFactor =  1.0;
               else
                  shadowFactor =  0.1;
            } else if ( j == 1 ) {
               toLightDirection = uPointLights[1].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uPointLights[1].intensity * attenuation;
               SampledDistance = 0.0;//texture(uPointShadowMap1, toLightDirection).r;
               Distance = length(toLightDirection);
               if (Distance <= SampledDistance + 0.001)
                  shadowFactor =  1.0;
               else
                  shadowFactor =  0.1;
            } else if ( j == 2 ) {
               toLightDirection = uPointLights[2].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uPointLights[2].intensity * attenuation;
               SampledDistance = 0.0;//texture(uPointShadowMap2, toLightDirection).r;
               Distance = length(toLightDirection);
               if (Distance <= SampledDistance + 0.001)
                  shadowFactor =  1.0;
               else
                  shadowFactor =  0.1;
            } else if ( j == 3 ) {
               toLightDirection = uPointLights[3].position.xyz - vVertexWorldSpace.xyz;
               toLightDirectionSqr = dot(toLightDirection, toLightDirection);
               attenuation = (1.0 / ( 1.0 + lightAttenuation * sqrt(toLightDirectionSqr)));
               intensity = uPointLights[3].intensity * attenuation;
               SampledDistance = 0.0;//texture(uPointShadowMap3, toLightDirection).r;
               Distance = length(toLightDirection);
               if (Distance <= SampledDistance + 0.001)
                  shadowFactor =  1.0;
               else
                  shadowFactor =  0.1;
            }
         }

         diffuseSum += 0.001*shadowFactor * intensity;
         specularSum += 0.001*shadowFactor * intensity;
      }

      diffuseSum.w = 0.0;
      specularSum.w = 0.0;

      colorDiffuse =  vec4 (ambientFactor, 1.0) * (diffuseSum + vColor);
      float boost = 5.0;
      color = ( vec4(boost * colorDiffuse.xyz, 1.0) + uLightAmbientIntensity ) * texelDiffuse + boost * specularSum;
      color.a = colorDiffuse.a;

      /* Effects */

      if ( uPainterEffect ){
         color = uPainterColor;
      } else if ( uTextureOnlyEffect ){
         color = texelDiffuse;
      } else if ( uBoostColorEffect ){
         color = vec4(uBoostColorFactor * color.x,
                      uBoostColorFactor * color.y,
                      uBoostColorFactor * color.z, 1.0) ;
      }

      if ( uFogEffect ) {
         float fogFactor = ( length(-vCameraPosition + vVertexWorldSpace.xyz) - uFogNearDistance) / (uFogFarDistance - uFogNearDistance);
         fogFactor = clamp (fogFactor, 0.0, 1.0);
         color = vec4(mix (color.rgb , uFogColor, fogFactor), 1.0);
      }
   }
   fColor = color;
}


