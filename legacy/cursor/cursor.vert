/* shader to update a particle system based on a simple kinematics function */
#version 330 core

//uniform mat4 uViewMatrix, uProjectionMatrix, uModelMatrix;
//uniform float uTimeElapsed;
uniform float uSize = 0.1;

uniform float uScreenFactor = 1.0;
uniform vec2 uPosition = vec2(0.0,0.0);

// the fragment shader can use this for it's output colour's alpha component 
//out float vOpacity;
//out vec4 vColor;
out vec2 vVertexOffset;

void main() {
   switch(gl_VertexID) {
      case 0:
         // 2D
         vVertexOffset = vec2(-1.0, 1.0);
         break;
      case 1:
         // 2D
         vVertexOffset = vec2(-1.0, -1.0);
         break;
      case 2:
         // 2D
         vVertexOffset = vec2(1.0, 1.0);
         break;
      case 3:
         // 2D
         vVertexOffset = vec2(1.0, -1.0);
      break;
   }

   gl_Position = vec4(uPosition.x + uSize * vVertexOffset.x, uPosition.y + uSize * vVertexOffset.y * uScreenFactor, 0.0, 1.0);
}
