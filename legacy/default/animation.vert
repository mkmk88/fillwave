#version 330

layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec3 aNormalTangent;
layout(location = 4) in vec2 aTextureCoordinate;
layout(location = 5) in ivec4 aBoneID; /*because we assume that the mesh depends on 4 bones*/
layout(location = 6) in vec4 aWeight; /*because we assume that the mesh depends on 4 bones*/

out vec4 vColor;
out vec2 vTextureCoordinate;
out vec3 vVertexNormal;
out vec3 vVertexNormalTangent;
out vec4 vVertexWorldSpace;
out vec3 vCameraPosition;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat3 uNormalViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 uColorMatrix;
uniform vec3 uCameraPosition;

uniform float uTime;

#define MAX_BONES 45

uniform mat4 uBones[MAX_BONES];

void main() {
   mat4 BoneTransform = uBones[aBoneID.x] * aWeight.x +
                        uBones[aBoneID.y] * aWeight.y +
                        uBones[aBoneID.z] * aWeight.z +
                        uBones[aBoneID.w] * aWeight.w;

   vVertexNormal = (uModelMatrix * BoneTransform * vec4(aNormal, 0.0)).xyz;
   vVertexNormalTangent = (uModelMatrix * BoneTransform * vec4(aNormalTangent, 0.0)).xyz;
   vVertexWorldSpace = uModelMatrix * BoneTransform * aPosition;
   vCameraPosition = uCameraPosition;
   vTextureCoordinate = aTextureCoordinate;
   vColor = aColor;
   gl_Position = uProjectionMatrix * uViewMatrix * vVertexWorldSpace;
}
