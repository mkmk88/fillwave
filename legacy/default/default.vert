#version 330

layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec3 aNormalTangent;
layout(location = 4) in vec2 aTextureCoordinate;
layout(location = 5) in ivec4 aBoneID; /* Will be optimized if not used */
layout(location = 6) in vec4 aWeight;  /* Will be optimized if not used */

/* ------------ BONES START --------------- */

out vec4 vColor;
out vec2 vTextureCoordinate;
out vec3 vVertexNormal; // N
out vec3 vVertexNormalTangent;
out vec4 vVertexWorldSpace;
out vec3 vCameraPosition;

uniform mat4 uModelMatrix = mat4(1.0);
uniform mat4 uViewMatrix = mat4(1.0);
uniform mat3 uNormalViewMatrix = mat3(1.0);
uniform mat4 uProjectionMatrix = mat4(1.0);
uniform mat4 uColorMatrix = mat4(1.0);
uniform mat4 uShadowBiasMatrix = mat4(1.0);
uniform vec3 uCameraPosition = vec3(0.0);

void main() {
   vVertexWorldSpace = uModelMatrix * aPosition;
   vVertexNormal = (uModelMatrix * vec4(aNormal, 0.0)).xyz;
   vVertexNormalTangent = (uModelMatrix * vec4(aNormalTangent, 0.0)).xyz;
   vTextureCoordinate = aTextureCoordinate;
   vCameraPosition = uCameraPosition;
   vColor = aColor;
   gl_Position = uProjectionMatrix * uViewMatrix * vVertexWorldSpace;
}
