#version 330 core
out vec2 vPosition;
void main() {
   vec4 vertexPosition;
   switch(gl_VertexID) {
   case 0:
      vertexPosition = vec4(-1.0, 1.0, 0.0,1.0);
      break;
   case 1:
      vertexPosition = vec4(-1.0, -1.0, 0.0,1.0);
      break;
   case 2:
      vertexPosition = vec4(1.0, 1.0, 0.0,1.0);
      break;
   case 3:
   default:
      vertexPosition = vec4(1.0, -1.0, 0.0,1.0);
      break;
   }
   gl_Position = vertexPosition;
   vPosition = vertexPosition.xy;
}
