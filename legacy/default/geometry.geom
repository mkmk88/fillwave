#version 330 

layout(triangles) in; 
layout(triangle_strip, max_vertices = 9)out; 

in vec2 vTextureCoordinate[]; // Input variable is array, one value for each vertex
in vec3 vNormal[];  // Notice that name of out variable in vertex shader must match
in mat4 vModelMatrix[];
in vec4 vColor[];

out vec3 gNormal; 
out vec2 gTextureCoordinate; 
out vec4 gColor; 

#define MAX_LIGHTS 4

struct Light {
   vec4 position;
   vec4 intensity;
};

uniform mat4 uViewMatrix = mat4(1.0);
uniform mat3 uNormaluViewMatrix  = mat3(1.0);;
uniform mat4 uProjectionMatrix  = mat4(1.0);;
uniform mat4 uModelMatrix  = mat4(1.0);;
uniform mat4 uColorMatrix  = mat4(1.0);;
uniform int  uNumberOfLights = 0;
uniform Light uLights[MAX_LIGHTS];

float fBender = 0.01; 

void main() {
  vec3 vertexNormal;  // N
  vec4 tmpColor;
  float cosAngIncidence;
  int iterator;
  float attenuation;
  vec3 dirToLight;
  vec4 vertexWorldspace;
  vec2 textureCoordinateMiddle;
  
  mat4 mMVP = uProjectionMatrix*uViewMatrix*uModelMatrix; 
  
  // Calculate the centroid point (just sum up all coordinates and divide by 3
  // You can see built-in variable gl_in here, notice adding normal multiplied by bender value
  
  vec3 positionMiddle = (gl_in[0].gl_Position.xyz + 
                gl_in[1].gl_Position.xyz + 
                gl_in[2].gl_Position.xyz)/3.0 + 
                   (vNormal[0] + 
                    vNormal[1] + 
                    vNormal[2])*fBender; 

  // Centroid coordinate is average of three as well
  textureCoordinateMiddle = (vTextureCoordinate[0] +
                         vTextureCoordinate[1] + 
                          vTextureCoordinate[2])/3.0; 
 
  // Transform normals of 3 triangle vertices with transform matrix and store them in this array
  vec3 normalTransformed[3]; 
  for(int i = 0; i < 3; i++){
     //normalTransformed[i] = (vec4(vNormal[i], 1.0)*uViewMatrix).xyz;
    normalTransformed[i] = (uModelMatrix*vec4(vNormal[i], 1.0)).xyz;
  }
    
  // Calculate centroid normal
  vec3 normalMiddle = (normalTransformed[0]+
                    normalTransformed[1]+
                    normalTransformed[2])/3.0; 

  vec4 vertexWorldspaceMiddle = uModelMatrix * vec4(positionMiddle,1.0);
  
  if (uNumberOfLights>MAX_LIGHTS)
    iterator = 1;
  else
   iterator = uNumberOfLights;

  tmpColor = vec4(0.0);
  for (int i = 0;i < iterator; i++)
    {
   dirToLight = uLights[i].position.xyz - vertexWorldspaceMiddle.xyz;
   cosAngIncidence = clamp( dot(normalMiddle, normalize ( dirToLight ) ), 0.0, 1.0);
   attenuation = 20.0 * float(uNumberOfLights)/length(dirToLight);
   tmpColor = tmpColor + uLights[i].intensity * cosAngIncidence * attenuation;
   }
  vec4 gColorMiddle = tmpColor + (vColor[0]+vColor[1]+vColor[2])/3.0;
                    
  for(int i = 0; i < 3; i++) 
  { 
    // Emit first vertex
    vec3 vPos = gl_in[i].gl_Position.xyz; 
    gl_Position = mMVP*vec4(vPos, 1.0); 
    gNormal = (vec4(normalTransformed[i], 1.0)).xyz; 
    gTextureCoordinate = vTextureCoordinate[i];
    
    vertexWorldspace = uModelMatrix * vec4(vPos,1.0);
  
     vertexNormal = normalize( mat3(uModelMatrix) * (vec4(normalTransformed[i], 1.0)).xyz);
  
     if (uNumberOfLights>MAX_LIGHTS)
       iterator = 1;
     else
      iterator = uNumberOfLights;

     tmpColor = vec4(0.0);
     for (int j = 0;j < iterator; j++)
       {
      dirToLight = uLights[j].position.xyz - vertexWorldspace.xyz;
      cosAngIncidence = clamp( dot(vertexNormal, normalize ( dirToLight ) ), 0.0, 1.0);
      attenuation = 20.0 * float(uNumberOfLights)/length(dirToLight);
      tmpColor = tmpColor + uLights[j].intensity * cosAngIncidence * attenuation;
      }
     gColor = tmpColor + vColor[i];
    EmitVertex(); 

    // Emit second vertex, that comes next in order
    vPos = gl_in[(i+1)%3].gl_Position.xyz; 
    gl_Position = mMVP*vec4(vPos, 1.0); 
    gNormal = (vec4(normalTransformed[(i+1)%3], 1.0)).xyz; 
    gTextureCoordinate = vTextureCoordinate[(i+1)%3];
    
    vertexWorldspace = uModelMatrix * vec4(vPos,1.0);
  
     vertexNormal = normalize( mat3(uModelMatrix) * normalTransformed[(i+1)%3]);
  
     if (uNumberOfLights>MAX_LIGHTS)
       iterator = 1;
     else
      iterator = uNumberOfLights;

     tmpColor = vec4(0.0);
     for (int j = 0;j < iterator; j++)
       {
      dirToLight = uLights[j].position.xyz - vertexWorldspace.xyz;
      cosAngIncidence = clamp( dot(vertexNormal, normalize ( dirToLight ) ), 0.0, 1.0);
      attenuation = 20.0 * float(uNumberOfLights)/length(dirToLight);
      tmpColor = tmpColor + uLights[j].intensity * cosAngIncidence * attenuation;
      }
     gColor = tmpColor + vColor[(i+1)%3];
     
    EmitVertex(); 

    // Emit third vertex - the centroid
    gl_Position = mMVP*vec4(positionMiddle, 1.0);
    gNormal = normalMiddle; 
    gTextureCoordinate = textureCoordinateMiddle;
     gColor = gColorMiddle;
    EmitVertex(); 

    EndPrimitive(); 
  } 
}