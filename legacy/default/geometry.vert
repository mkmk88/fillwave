#version 330

layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec2 aTextureCoordinate;

out vec4 vColor;
out vec2 vTextureCoordinate;  // to geometry shader
out vec3 vNormal;

void main() {
   vTextureCoordinate = aTextureCoordinate;
   vColor = aColor;
   gl_Position = aPosition;
   vNormal = aNormal;
}