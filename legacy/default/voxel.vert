#version 330

layout(location = 0) in vec4 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTextureCoordinate;

/* ------------ MATERIAL START --------------- */
struct Material {
    float ambient[3];
    float diffuse[3];
    float specular[3];
    float transmittance[3];
    float emission[3];
    float shininess;
    float ior;                // index of refraction
};
/* ------------ MATERIAL STOP --------------- */

/* ------------ BONES START --------------- */

out vec4 vColor;
out vec2 vTextureCoordinate; //dokonczyc zmiane dazw w zykłych shaderach
out vec3 vVertexNormal; // N
out vec3 vViewPosition; // v
out vec4 vVertexWorldSpace;
out vec3 vCameraPosition;

uniform mat4 uModelMatrix = mat4(1.0);
uniform mat4 uViewMatrix = mat4(1.0);
uniform mat3 uNormalViewMatrix = mat3(1.0);
uniform mat4 uProjectionMatrix = mat4(1.0);
uniform mat4 uColorMatrix = mat4(1.0);

//needed by specular light component, fog calculations, and terrain picking
uniform vec3 uCameraPosition = vec3(0.0);

uniform Material uMaterial;

void main() {

   vec4 vertexWorldSpace = uModelMatrix * aPosition;

   vVertexNormal = normalize( ( uModelMatrix * vec4(aNormal, 0.0)).xyz );
   vVertexWorldSpace = vertexWorldSpace;
   vViewPosition = (uViewMatrix * uModelMatrix * aPosition).xyz;
   vTextureCoordinate = aTextureCoordinate;
   vCameraPosition = uCameraPosition;
   vColor = vec4(0.0,0.0,0.0,1.0);
   gl_Position = uProjectionMatrix * uViewMatrix * vertexWorldSpace;
}