#version 300 es

struct Attenuation {
    float Constant;
    float Linear;
    float Exp;
};

 struct Light {
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
 };

struct LightDirectional {
    Light base;
    Attenuation attenuation;
    vec3 direction;
};

uniform sampler2D uShadowMap;
uniform sampler2D uWorldPositionAttachment;
uniform sampler2D uDiffuseTexelAttachment;
uniform sampler2D uNormalAttachment;
uniform sampler2D uSpecularTexelAttachment;

uniform LightDirectional uLight;
uniform vec3 uCameraPosition;
uniform float uSpecularPower;
uniform vec2 uScreenSize;

vec4 CalcLightInternal(Light light,
					   vec3 lightDirection,
					   vec3 WorldPos,
					   vec3 Normal,
					   float specularTexel)
{
    vec4 AmbientColor = vec4(light.Color, 1.0) * light.AmbientIntensity;
    float DiffuseFactor = dot(Normal, -lightDirection);

    vec4 DiffuseColor  = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0.0) {
        DiffuseColor = vec4(light.Color, 1.0) * light.DiffuseIntensity * DiffuseFactor;

        vec3 VertexToEye = normalize(uCameraPosition - WorldPos);
        vec3 LightReflect = normalize(reflect(lightDirection, Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, uSpecularPower);
        if (SpecularFactor > 0.0) {
            SpecularColor = vec4(light.Color, 1.0) * specularTexel * SpecularFactor;
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcDirectionalLight(vec3 WorldPos, vec3 Normal, float speculatTexel)
{
    return CalcLightInternal(uLight.base,
							 uLight.direction,
							 WorldPos,
							 Normal,
							 speculatTexel);
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / uScreenSize;
}

out vec4 FragColor;

void main()
{
    vec2 TexCoord = CalcTexCoord();
	vec3 WorldPos = texture(uWorldPositionAttachment, TexCoord).xyz;
	vec3 Color = texture(uDiffuseTexelAttachment, TexCoord).xyz;
	vec3 Normal = texture(uNormalAttachment, TexCoord).xyz;
    float specularTexel = texture(uSpecularTexelAttachment, TexCoord).x;
	Normal = normalize(Normal);

	FragColor = vec4(Color, 1.0) * CalcDirectionalLight(WorldPos, Normal, specularTexel);
}
