#version 330

layout (location = 0) in vec2 aPosition;
layout (location = 1) in vec2 aTextureCoordinate;

out vec2 vTextureCoordinate;
out vec2 vPosition;

void main () {
  vTextureCoordinate = aTextureCoordinate;
  vPosition = aPosition;
  gl_Position = vec4 (aPosition, 0.0, 1.0);
}