/* shader to render simple particle system points */
#version 330 core

in float vOpacity;
out vec4 fColor;

uniform sampler2D uTextureUnit;
uniform vec4 uColor; 

void main () {
	vec4 texel = texture (uTextureUnit, gl_PointCoord);
	fColor = texel*uColor;
}