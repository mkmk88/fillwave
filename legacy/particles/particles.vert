/* shader to update a particle system based on a simple kinematics function */
#version 330 core

layout (location = 0) in vec3 aVelocity;
layout (location = 1) in vec3 aStartPosition;
layout (location = 2) in float aStartTime;

uniform mat4 uViewMatrix, uProjectionMatrix;
uniform vec3 uSourcePosition; // source position in world space
uniform float uTimeElapsed;   // time in seconds
uniform float uPointSize;   // time in seconds
uniform float uLifeTime;   // time in seconds

uniform vec3 uAcceleration;   // 2nd time integral
uniform vec3 uCameraPosition; // 2nd time integral

// the fragment shader can use this for it's output colour's alpha component 
out float vOpacity;

void main() {

	float time = mod ( uTimeElapsed - aStartTime, uLifeTime );

	vOpacity = 0.0;

	vec3 p = uSourcePosition;
	
	// acceleration
	// vec3 a = vec3 (0.0, 1.0, 0.0);

    //position
	p += aStartPosition + aVelocity * time + 0.5 * uAcceleration * time * time;
	
	// Fade to invisible after 6 seconds
	vOpacity = 1.0 - (time / 6.0);
	
	gl_Position = uProjectionMatrix * uViewMatrix * vec4 (p, 1.0);
	gl_PointSize = uPointSize/(length(aStartPosition - uCameraPosition )); // size in pixels
}
