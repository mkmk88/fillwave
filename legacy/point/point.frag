#version 330

in vec2 vTextureCoordinate;
out vec4 fColor;

uniform sampler2D uTextureUnit;

void main() {
   fColor = texture(uTextureUnit, vTextureCoordinate);
}
