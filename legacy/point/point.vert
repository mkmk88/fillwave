#version 330

in vec4 aPosition;
in vec2 aTextureCoordinate;
out vec2 vTextureCoordinate;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;
uniform float uPointSize;

void main() {
   gl_PointSize = uPointSize;
   gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aPosition;
   vTextureCoordinate = aTextureCoordinate;
}