#version 330                                                                        

in vec3 vTextureCoordinate;
            
uniform samplerCube uTextureUnit;

out vec4 fColor;

void main() {
    fColor = texture(uTextureUnit, vTextureCoordinate);
}