#version 330

layout(location = 0) in vec4 aPosition;

uniform mat4 uModelMatrix = mat4(1.0);
uniform mat4 uViewMatrix = mat4(1.0);
uniform mat4 uProjectionMatrix = mat4(1.0);
uniform vec3 uCameraPosition = vec3(0.0);
 
out vec3 vTextureCoordinate;

void main() {
   vec4 position = uProjectionMatrix * 
                   uViewMatrix *
                   vec4((uModelMatrix * aPosition).xyz + uCameraPosition, aPosition.a);
  // position.z = -1.0;
   gl_Position = position;
   vTextureCoordinate = aPosition.xyz;
}