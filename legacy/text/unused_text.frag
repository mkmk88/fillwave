#version 330

in vec2 vTextureCoordinate;

uniform sampler2D uTextureUnit;
uniform vec4 uColour;

out vec4 fColor;

void main () {
  fColor = texture (uTextureUnit, vTextureCoordinate) * uColour;
}