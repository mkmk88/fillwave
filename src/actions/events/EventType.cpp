/*
 * EventType.cpp
 *
 *  Created on: Oct 1, 2014
 *      Author: filip
 */

#include <fillwave/actions/EventType.h>

namespace fillwave {
namespace actions {

EventType::EventType(eEventType type)
:mType(type) {

}

EventType::~EventType() {

}

} /* actions */
} /* fillwave */
