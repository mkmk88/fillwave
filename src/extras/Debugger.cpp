/*
 * Debugger.cpp
 *
 *  Created on: Jul 26, 2014
 *      Author: filip
 */

/*************************************************************************
 *
 * Copyright (C) 2014 Filip Wasil
 *
 * Filip Wasil CONFIDENTIAL
 * __________________
 *
 *  [2012] - [2014] Filip Wasil
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and his suppliers and may be covered by Polish and Foreign
 * Patents, patents in process, and are protected by trade secret
 * or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * To use the code, you must contact the author directly and ask permission.
 *
 * filip.wasil@gmail.com
 *
 */

#include <fillwave/Fillwave.h>

#include <fillwave/core/buffers/VertexBufferDebug.h>

#include <fillwave/extras/Debugger.h>

#include <fillwave/space/Camera.h>

#include <fillwave/management/LightManager.h>

#include <fillwave/loaders/ProgramLoader.h>

#include <fillwave/extras/Log.h>

namespace fillwave {

FLOGINIT("Debugger", FERROR | FFATAL | FINFO)

Debugger::Debugger(Engine* engine)
:Reloadable(engine),
 mState(eDebuggerState::off),
 mEngine(engine),
 mVBO(pVertexBufferDebug (new core::VertexBufferDebug(1.0))),
                              mMiniwindowSize(1.0/6.0),
                              mMiniwindowsOccupied(0){

   loader::ProgramLoader loader;

   mProgram = loader.getDebugger(engine);

   initPipeline();
   initVBO();
   initVAO();
   initUniformsCache();
}

Debugger::~Debugger() {

}

void Debugger::setState(eDebuggerState state) {
   if (state == eDebuggerState::toggleState) {
      switch (mState) {
         case eDebuggerState::lightsSpot:
            mState = eDebuggerState::lightsSpotDepth;
//            std::cout << "eDebuggerState::lightsSpotDepth" << std::endl;
            break;
         case eDebuggerState::lightsSpotDepth:
            mState = eDebuggerState::lightsSpotColor;
//            std::cout << "eDebuggerState::lightsSpotColor" << std::endl;
            break;
         case eDebuggerState::lightsSpotColor:
            mState = eDebuggerState::lightsPoint;
//            std::cout << "eDebuggerState::lightsPoint" << std::endl;
            break;
         case eDebuggerState::lightsPoint:
            mState = eDebuggerState::lightsPointDepth;
//            std::cout << "eDebuggerState::lightsPointDepth" << std::endl;
            break;
         case eDebuggerState::lightsPointDepth:
            mState = eDebuggerState::lightsPointColor;
//            std::cout << "eDebuggerState::lightsPointColor" << std::endl;
            break;
         case eDebuggerState::lightsPointColor:
            mState = eDebuggerState::pickingMap;
//            std::cout << "eDebuggerState::pickingMap" << std::endl;
            break;
         case eDebuggerState::pickingMap:
            mState = eDebuggerState::off;
//            std::cout << "eDebuggerState::off" << std::endl;
            break;
         case eDebuggerState::off:
            mState = eDebuggerState::lightsSpot;
//            std::cout << "eDebuggerState::lightsSpot" << std::endl;
            break;
         default:
//            std::cout << "eDebuggerState::default" << std::endl;
            break;
      }
   } else {
      mState = state;
   }
}

eDebuggerState Debugger::getState() {
   return mState;
}

void Debugger::setMiniwindowSize(GLfloat size) {
   mMiniwindowSize = size;
}

void Debugger::renderFromCamera(space::Camera& c, GLint id) {
   glViewport (mEngine->getScreenSize()[0]*(id)*mMiniwindowSize,
       mEngine->getScreenSize()[1]*(1.0-mMiniwindowSize),
       mEngine->getScreenSize()[0]*mMiniwindowSize,
       mEngine->getScreenSize()[1]*mMiniwindowSize);

   glClear(GL_DEPTH_BUFFER_BIT);

   mEngine->getCurrentScene()->drawFromCustomCamera(c);

   glViewport (0, 0, mEngine->getScreenSize()[0], mEngine->getScreenSize()[1]);
}

void Debugger::renderPickingMap() {
   glViewport (mEngine->getScreenSize()[0]*(1.0 - mMiniwindowSize),
       0,
       mEngine->getScreenSize()[0]*mMiniwindowSize,
       mEngine->getScreenSize()[1]*mMiniwindowSize);

   glClear(GL_DEPTH_BUFFER_BIT);
   mEngine->getCurrentScene()->drawPicking();
   glViewport (0, 0, mEngine->getScreenSize()[0], mEngine->getScreenSize()[1]);
}

void Debugger::renderDepthOrthographic(GLint id) { //xxx ujednolicić to całe lightID żeby można było usuwać światła
   glViewport (mEngine->getScreenSize()[0]*(id)*mMiniwindowSize,
       0,
       mEngine->getScreenSize()[0]*mMiniwindowSize,
       mEngine->getScreenSize()[1]*mMiniwindowSize);

   glClear(GL_DEPTH_BUFFER_BIT);

   mProgram->use();

   mVAO->bind();

   pLightDirectional light = mEngine->getLightManager()->getLightDirectional(id - mEngine->getLightManager()->getLightsSpotHowMany());

   space::Camera* cam = light->getShadowCamera().get();

   light->getShadowTexture()->bind(GLint(FILLWAVE_SHADOW_FIRST_UNIT+id));

   glDisable (GL_DEPTH_TEST);
   glEnable (GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   core::Uniform::push(mUniformLocationCacheTextureUnit, id);
   core::Uniform::push(mUniformLocationCacheNearPlane, cam->getProjectionNearPlane());
   core::Uniform::push(mUniformLocationCacheFarPlane, cam->getProjectionFarPlane());

   glDrawArrays(GL_TRIANGLES, 0, mVBO->getElements());

   glEnable (GL_DEPTH_TEST);
   glDisable (GL_BLEND);

   core::Texture2D::unbind2DTextures();
   mVAO->unbind();
   core::Program::disusePrograms();

   glViewport (0, 0, mEngine->getScreenSize()[0], mEngine->getScreenSize()[1]);
}

void Debugger::renderDepthPerspective(GLint id) { //xxx ujednolicić to całe lightID żeby można było usuwać światła
   glViewport (mEngine->getScreenSize()[0]*(id)*mMiniwindowSize,
       0,
       mEngine->getScreenSize()[0]*mMiniwindowSize,
       mEngine->getScreenSize()[1]*mMiniwindowSize);

   glClear(GL_DEPTH_BUFFER_BIT);

   mProgram->use();

   mVAO->bind();

   pLightSpot light = mEngine->getLightManager()->getLightSpot(id);

   space::CameraPerspective cam = *(light->getShadowCamera().get());

   light->getShadowTexture()->bind(GLint(FILLWAVE_SHADOW_FIRST_UNIT+id));

   glDisable (GL_DEPTH_TEST);
   glEnable (GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   core::Uniform::push(mUniformLocationCacheTextureUnit, id);
   core::Uniform::push(mUniformLocationCacheNearPlane, cam.getProjectionNearPlane());
   core::Uniform::push(mUniformLocationCacheFarPlane, cam.getProjectionFarPlane());

   glDrawArrays (GL_TRIANGLES, 0, mVBO->getElements());

   glEnable (GL_DEPTH_TEST);
   glDisable (GL_BLEND);

   core::Texture2D::unbind2DTextures();
   mVAO->unbind();
   core::Program::disusePrograms();

   glViewport (0, 0, mEngine->getScreenSize()[0], mEngine->getScreenSize()[1]);
}

void Debugger::renderGeometryBuffer(GLuint width,
   	                  GLuint height,
                      GLuint attachments,
                      core::FramebufferGeometry* buffer) {
//   buffer->bindForWriting();
//   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//   mEngine->getCurrentScene()->draw();
//   core::Framebuffer::bindScreenFramebufferForWriting();

   GLuint mDebugAttachmentScreens[4][4] = {
      {0,0,width/2,height/2}, // position
      {0,height/2,width/2,height}, // color
      {width/2,height/2,width,height}, // normal
      {width/2,0,width,height/2} // specular
   };

   /* Move to right up corner*/
   for (int i = 0;i<4;i++) {
      mDebugAttachmentScreens[i][0] = (mDebugAttachmentScreens[i][0] + width)/2;
      mDebugAttachmentScreens[i][1] = (mDebugAttachmentScreens[i][1] + height)/2;
      mDebugAttachmentScreens[i][2] = (mDebugAttachmentScreens[i][2] + width)/2;
      mDebugAttachmentScreens[i][3] = (mDebugAttachmentScreens[i][3] + height)/2;
   }

   core::Framebuffer::bindScreenFramebufferForWriting();
//   glDrawBuffer(GL_COLOR_ATTACHMENT0);
   for(GLint i = 0; i < attachments; i++) {
      buffer->bindForReading(); /* We will bind GBuffer for reading ...*/
      buffer->setReadColorAttachment(i); /* .. and take color attachment color i 0 (out location 0 from fragment shader) ... */
      glBlitFramebuffer(0,
                   0,
                   width,
                   height,
                   mDebugAttachmentScreens[i][0],
                   mDebugAttachmentScreens[i][1],
                   mDebugAttachmentScreens[i][2],
                   mDebugAttachmentScreens[i][3],
                   GL_COLOR_BUFFER_BIT,
                   GL_LINEAR); /* ... to finally copy it into main framebuffer */
   }
   core::Framebuffer::bindScreenFramebuffer();
}

inline void Debugger::initBuffers() {
   if (mVBO) {
      mVBO->reload();
   }
}

inline void Debugger::initPipeline() {

}

inline void Debugger::initUniformsCache() {
   mUniformLocationCacheTextureUnit = mProgram->getUniformLocation("uTextureUnit");
   mUniformLocationCacheNearPlane = mProgram->getUniformLocation("uNearplane");
   mUniformLocationCacheFarPlane = mProgram->getUniformLocation("uFarPlane");
}

inline void Debugger::initVAO() {
   mSampler->bind();
   mVAO->bind();

   mVBO->bind();
   mVBO->setReady();
   mVBO->send();
   mVBO->attributesSetForVAO();

   mVAO->unbind();
}

inline void Debugger::initVBO() {
   mVBO->getAttributes(mProgram->getHandle());
   mVBO->attributesBind(mProgram);
}

} /* fillwave */

