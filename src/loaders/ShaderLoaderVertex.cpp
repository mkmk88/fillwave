/*
 * ShaderLoaderVertex.cpp
 *
 *  Created on: 13 May 2015
 *      Author: Filip Wasil
 */

/*************************************************************************
 *
 * Copyright (C) 2015 Filip Wasil
 *
 * Filip Wasil CONFIDENTIAL
 * __________________
 *
 *  [2012] - [2015] Filip Wasil
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and his suppliers and may be covered by Polish and Foreign
 * Patents, patents in process, and are protected by trade secret
 * or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * To use the code, you must contact the author directly and ask permission.
 *
 * fillwave@gmail.com
 *
 */

#include <fillwave/loaders/ShaderLoaderVertex.h>

namespace fillwave {
namespace loader {

ShaderLoaderVertex::ShaderLoaderVertex(eShaderLoaderOpenGLVersion version,
	      eShaderLoaderPrecision precisionInt,
	      eShaderLoaderPrecision precisionFloat,
	      eShaderLoaderVertexBones bones):ShaderLoader(version,
	    		                                       precisionInt,
	    		                                       precisionFloat),
	    		                          mBones(bones) {

}

ShaderLoaderVertex::~ShaderLoaderVertex() {

}

const std::string ShaderLoaderVertex::getSource() {

   std::string version =

   (mVersion == eShaderLoaderOpenGLVersion::OpenGL ? "#version 330 core\n" : "#version 300 es\n\n");

   std::string attributes =

   "layout(location = 0) in vec4 aPosition;\n"
   "layout(location = 1) in vec4 aColor;\n"
   "layout(location = 2) in vec3 aNormal;\n"
   "layout(location = 3) in vec3 aNormalTangent;\n"
   "layout(location = 4) in vec2 aTextureCoordinate;\n"
   "layout(location = 5) in ivec4 aBoneID; /* Will be optimized if not used */\n"
   "layout(location = 6) in vec4 aWeight;  /* Will be optimized if not used */\n";

   std::string outputs =

   "out vec4 vColor;\n"
   "out vec2 vTextureCoordinate;\n"
   "out vec3 vVertexNormal; // N\n"
   "out vec3 vVertexNormalTangent;\n"
   "out vec4 vVertexWorldSpace;\n"
   "out vec3 vCameraPosition;\n\n";

   std::string uniforms =

   "uniform mat4 uModelMatrix;\n"
   "uniform mat4 uViewProjectionMatrix;\n"
   "uniform mat3 uNormalViewMatrix;\n"
   "uniform mat4 uColorMatrix;\n"
   "uniform mat4 uShadowBiasMatrix;\n"
   "uniform vec3 uCameraPosition;\n\n";

   std::string uniformsBones =

   "uniform float uTime;\n"

   "#define MAX_BONES 45\n"

   "uniform mat4 uBones[MAX_BONES];\n";

   std::string mainStart =

   "void main() {\n";

   std::string mainBones =

   "   mat4 BoneTransform = uBones[aBoneID.x] * aWeight.x +\n"
   "      uBones[aBoneID.y] * aWeight.y +\n"
   "      uBones[aBoneID.z] * aWeight.z +\n"
   "      uBones[aBoneID.w] * aWeight.w;\n\n";

   std::string mainComputations =

   "   vVertexWorldSpace = uModelMatrix * aPosition;\n"
   "   vVertexNormal = (uModelMatrix * vec4(aNormal, 0.0)).xyz;\n"
   "   vVertexNormalTangent = (uModelMatrix * vec4(aNormalTangent, 0.0)).xyz;\n"
   "   vTextureCoordinate = aTextureCoordinate;\n"
   "   vCameraPosition = uCameraPosition;\n"
   "   vColor = aColor;\n"
   "   gl_Position = uViewProjectionMatrix * vVertexWorldSpace;\n";

   std::string mainComputationsWithBones =

   "   vVertexNormal = (uModelMatrix * BoneTransform * vec4(aNormal, 0.0)).xyz;\n"
   "   vVertexNormalTangent = (uModelMatrix * BoneTransform * vec4(aNormalTangent, 0.0)).xyz;\n"
   "   vVertexWorldSpace = uModelMatrix * BoneTransform * aPosition;\n"
   "   vCameraPosition = uCameraPosition;\n"
   "    vTextureCoordinate = aTextureCoordinate;\n"
   "   vColor = aColor;\n"
   "   gl_Position = uViewProjectionMatrix * vVertexWorldSpace;\n";

   std::string mainEnd = "}\n";

   std::string main;
   if (mBones == eShaderLoaderVertexBones::_0 ) {
      main = mainStart + mainComputations + mainEnd;
   } else {
      main = uniformsBones + mainStart + mainBones + mainComputationsWithBones + mainEnd;
   }

   return version + attributes + outputs + uniforms + main;
}

} /* loader */
} /* fillwave */
