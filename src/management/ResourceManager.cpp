/*************************************************************************
 *
 * Copyright (C) 2014 Filip Wasil
 *
 * Filip Wasil CONFIDENTIAL
 * __________________
 *
 *  [2012] - [2014] Filip Wasil
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Filip Wasil. The intellectual and technical
 * concepts contained herein are proprietary to Filip Wasil
 * and his suppliers and may be covered by Polish and Foreign
 * Patents, patents in process, and are protected by trade secret
 * or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written
 * permission is obtained from Filip Wasil.
 *
 * To use the code, you must contact the author directly and ask permission.
 *
 * filip.wasil@gmail.com
 *
 */


#include <fillwave/management/ResourceManager.h>

FLOGINIT("ResourceManager", FERROR | FFATAL)

namespace fillwave {
namespace manager {

ResourceManager::ResourceManager(std::string& rootPath) {
   mBufferManager   = puBufferManager(new manager::BufferManager());
   mProgramManager  = puProgramManager(new manager::ProgramManager());
   mTextureManager  = puTextureManager(new manager::TextureManager(rootPath));
   mShaderManager   = puShaderManager(new manager::ShaderManager(rootPath));
   mSamplerManager  = puSamplerManager(new manager::SamplerManager());
}

void ResourceManager::initBufferManager() {

}

void ResourceManager::initProgramManager() {

}

void ResourceManager::initShapeManager() {

}

void ResourceManager::initTextureManager() {

}

void ResourceManager::initShaderManager() {

}

void ResourceManager::initSamplerManager() {

}

pBuffer ResourceManager::getBuffer() {
   return pBuffer();
}

pProgram ResourceManager::getProgram() {
   return pProgram();
}

pTexture2D ResourceManager::getTexture2D() {
   return pTexture2D();
}

pShader ResourceManager::getShader() {
   return pShader();
}

pSampler ResourceManager::getSampler() {
   return pSampler();
}

ResourceManager::~ResourceManager() {

}

} /* manager */
} /* fillwave */
