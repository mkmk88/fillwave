/*
 * TerrainConstructor.cpp
 *
 *  Created on: Apr 20, 2015
 *      Author: filip
 */

#include <fillwave/terrain/TerrainConstructor.h>
#include <fillwave/extras/Log.h>

FLOGINIT("TerrainConstructor", FERROR | FFATAL)

namespace fillwave {
namespace terrain {

TerrainConstructor::TerrainConstructor() {
}

TerrainConstructor::~TerrainConstructor() {
}

} /* models */
} /* fillwave */
