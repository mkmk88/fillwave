//============================================================================
// Name        : example_shadow.cpp
// Author      : Filip Wasil
// Version     :
// Copyright   : none
// Description : Fillwave engine example shadow
//============================================================================

#include <example.h>

/* Audio */
//#include <portaudio.h>

/* Graphics */
#include <CallbacksGLFW/MoveCameraCallback.h>
#include <CallbacksGLFW/AnimationKeyboardCallback.h>
#include <CallbacksGLFW/TimeStopCallback.h>
#include <ContextGLFW.h>
#include <fillwave/Fillwave.h>

/* Physics */
//#include <bullet>

using namespace fillwave;
using namespace std;

Engine* gEngine;

pScenePerspective gScene;
pCameraPerspective gCamera;

map<string, pEntity> gEntities;
map<string, pProgram> gPrograms;

#ifdef __linux__
   void junk() {
     int i;
     i=pthread_getconcurrency();
   };
#elif _WIN32
    // windows code goes here
#endif

int main(int argc, char* argv[]) {
   ContextGLFW mContext;
   ContextGLFW::mGraphicsEngine = new Engine(argc, argv);
   gEngine = ContextGLFW::mGraphicsEngine;
   ContextGLFW::mGraphicsEngine->insertResizeScreen(mContext.getScreenWidth(),
                                                    mContext.getScreenHeight());
   init();
   perform();
   showDescription();
   mContext.render();
   delete gEngine;
   exit(EXIT_SUCCESS);
}

void init() {
   /* Scene */
   gScene = buildScenePerspective();

   /* Camera */
   gCamera = pCameraPerspective ( new space::CameraPerspective(glm::vec3(0.0,0.0,6.0),
                                                    glm::quat(),
                                                    glm::radians(90.0),
                                                    1.0,
                                                    0.1,
                                                    1000.0));
   /* Programs */
   loader::ProgramLoader loader;
   gPrograms.insert(pair<string,pProgram>("default", loader.getDefault(gEngine)));

   /* Entities */
   gEntities.insert(pair<string,pEntity>("wall", buildEntity()));
   gEntities.insert(pair<string,pEntity>("lightSource_1", buildEntity()));
   gEntities.insert(pair<string,pEntity>("lightSource_2", buildEntity()));
   gEntities.insert(pair<string,pEntity>("lightSource_3", buildEntity()));
   gEntities.insert(pair<string,pEntity>("shadowCastingBall1", buildEntity()));
   gEntities.insert(pair<string,pEntity>("shadowCastingBall2", buildEntity()));
   gEntities.insert(pair<string,pEntity>("shadowCastingBall3", buildEntity()));
   gEntities.insert(pair<string,pEntity>("shadowCastingBall4", buildEntity()));
   gEntities.insert(pair<string,pEntity>("shadowCastingBall5", buildEntity()));

   /* Lights */
//   gEngine->storeLightPoint(glm::vec3 (0.0,1.0,0.0),
//                           glm::vec4 (0.0,1.0,0.0,0.0),
//                           gEntities["lightSource_1"] );
//
//   gEngine->storeLightPoint(glm::vec3 (0.0,1.0,0.0),
//                           glm::vec4 (0.0,0.0,1.0,0.0),
//                           gEntities["lightSource_2"] );
//
//   gEngine->storeLightPoint(glm::vec3 (1.0,1.0,1.0),
//                           glm::vec4 (1.0,0.0,0.0,0.0),
//                           gEntities["lightSource_3"] );

//   gEngine->storeLightSpot(glm::vec3 (0.0,1.0,0.0),
//                           glm::quat(),
//                           glm::vec4 (0.0,1.0,0.0,0.0),
//                           gEntities["lightSource_1"] );
//
//   gEngine->storeLightSpot(glm::vec3 (0.0,1.0,0.0),glm::quat(),
//                           glm::vec4 (0.0,0.0,1.0,0.0),
//                           gEntities["lightSource_2"] );
//
//   gEngine->storeLightSpot(glm::vec3 (1.0,1.0,1.0),glm::quat(),
//                           glm::vec4 (1.0,0.0,0.0,0.0),
//                           gEntities["lightSource_3"] );

   gEngine->storeLightDirectional(glm::vec3 (0.0,1.0,0.0),
                           glm::quat(),
                           glm::vec4 (0.0,1.0,0.0,0.0),
                           gEntities["lightSource_1"] );

   gEngine->storeLightDirectional(glm::vec3 (0.0,1.0,0.0),glm::quat(),
                           glm::vec4 (0.0,0.0,1.0,0.0),
                           gEntities["lightSource_2"] );

   gEngine->storeLightDirectional(glm::vec3 (1.0,1.0,1.0),glm::quat(),
                           glm::vec4 (1.0,0.0,0.0,0.0),
                           gEntities["lightSource_3"] );

   /* Engine callbacks */
   gEngine->registerKeyCallback(new actions::TimeStopCallback());
   gEngine->registerKeyCallback(new actions::MoveCameraCallback(eEventType::key, 0.1));
   gEngine->registerCursorPositionCallback(new actions::MoveCameraCallback(eEventType::cursorPosition, 0.1, ContextGLFW::mWindow));
}

void perform() {
   /* Set scene */
   gEngine->setCurrentScene(gScene);

   /* Attach entities to scene */
   gScene->attach(gEntities["wall"] );
   gScene->attach(gEntities["lightSource_1"] );
   gScene->attach(gEntities["lightSource_2"] );
   gScene->attach(gEntities["lightSource_3"] );
   gScene->attach(gEntities["shadowCastingBall1"] );
   gScene->attach(gEntities["shadowCastingBall2"] );
   gScene->attach(gEntities["shadowCastingBall3"] );
   gScene->attach(gEntities["shadowCastingBall4"] );
   gScene->attach(gEntities["shadowCastingBall5"] );

   /* Attach camera to scene */
   gScene->setCamera(gCamera);

   gEntities["lightSource_1"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "0_255_0.color"));
   gEntities["lightSource_2"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "0_0_255.color"));
   gEntities["lightSource_3"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "255_0_0.color"));
//   gEntities["shadowCastingBall1"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "64_128_255.checkboard"));
//   gEntities["shadowCastingBall2"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "64_128_255.checkboard"));
//   gEntities["shadowCastingBall3"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "64_128_255.checkboard"));
//   gEntities["shadowCastingBall4"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "64_128_255.checkboard"));
//   gEntities["shadowCastingBall5"]->attach(buildModel(gEngine, gPrograms["default"], "meshes/sphere.obj", "64_128_255.checkboard"));
   pModel wall = buildModel(gEngine, gPrograms["default"], "meshes/floor.obj");
   gEntities["wall"]->attach(wall);

   /* Manipulate objects directly */
   gEntities["wall"]->rotateByX(glm::radians(90.0));
   gEntities["wall"]->moveInDirection(glm::vec3(0.0,-10.0,0.0));
   gEntities["wall"]->scaleTo(3.0);

   /* Manipulate objects, set Callbacks */
   gEntities["shadowCastingBall1"]->scaleTo(0.1);
   gEntities["shadowCastingBall1"]->moveBy(glm::vec3(-2.0,0.0,0.0));

   gEntities["shadowCastingBall2"]->scaleTo(0.1);
   gEntities["shadowCastingBall2"]->moveBy(glm::vec3(0.0,0.0,3.0));

   gEntities["shadowCastingBall3"]->scaleTo(0.1);
   gEntities["shadowCastingBall3"]->moveBy(glm::vec3(2.0,0.0,3.0));

   gEntities["shadowCastingBall4"]->scaleTo(0.1);
   gEntities["shadowCastingBall4"]->moveBy(glm::vec3(0.0,2.0,3.0));

   gEntities["shadowCastingBall5"]->scaleTo(0.1);
   gEntities["shadowCastingBall5"]->attachHierarchyCallback(new actions::TimedMoveCallback(gEntities["shadowCastingBall5"],
                                                            glm::vec3(0.0,0.0,3.0),
                                                            2000.0));

   gEntities["shadowCastingBall5"]->moveBy(glm::vec3(0.0,-2.0,3.0));

   gEntities["lightSource_1"]->attachHierarchyCallback(new actions::TimedMoveCallback(gEntities["lightSource_1"], glm::vec3(0.0,0.0,500.0), 2000.0));
   gEntities["lightSource_1"]->scaleTo(0.02);
   gEntities["lightSource_1"]->moveBy(glm::vec3(0.0,0.0,0.0));

   gEntities["lightSource_2"]->attachHierarchyCallback(new actions::TimedMoveCallback(gEntities["lightSource_2"], glm::vec3(0.0,0.0,500.0), 2000.0));
   gEntities["lightSource_2"]->scaleTo(0.02);
   gEntities["lightSource_2"]->moveBy(glm::vec3(3.0,0.0,0.0));

   gEntities["lightSource_3"]->attachHierarchyCallback(new actions::TimedMoveCallback(gEntities["lightSource_3"], glm::vec3(0.0,0.0,500.0), 2000.0));
   gEntities["lightSource_3"]->scaleTo(0.02);
   gEntities["lightSource_3"]->moveBy(glm::vec3(-3.0,0.0,0.0));
}

void showDescription() {
   pText hint0 = gEngine->storeText("Fillwave example shadowing", "fonts/Titania", -0.95, 0.80, 100.0);
   pText hint5 = gEngine->storeText("Use mouse to move the camera", "fonts/Titania", -0.95, -0.40, 70.0);
   pText hint3 = gEngine->storeText("Use 'S' for camera back", "fonts/Titania", -0.95, -0.50, 70.0);
   pText hint4 = gEngine->storeText("Use 'W' for camera forward", "fonts/Titania", -0.95, -0.60, 70.0);
   pText hint1 = gEngine->storeText("Use 'T' to resume/stop time", "fonts/Titania", -0.95, -0.70, 70.0);
   pText hint6 = gEngine->storeText("Use 'D' for toggle debugger On/Off", "fonts/Titania", -0.95, -0.80, 70.0);

}
